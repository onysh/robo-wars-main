import cv2
import argparse
from imutils.video import FileVideoStream
import numpy as np

ap = argparse.ArgumentParser()
ap.add_argument("-video", type=str, help="Video file location")
args = vars(ap.parse_args())

CB_WIDTH = 6
CB_HEIGHT = 7

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((CB_WIDTH*CB_HEIGHT,3), np.float32)
objp[:,:2] = np.mgrid[0:CB_HEIGHT,0:CB_WIDTH].T.reshape(-1,2)
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

fvs = FileVideoStream(args["video"]).start()
cv2.imshow('img', fvs.read())
numFrame = 1
numRecognized = 1
numScreens = 1
while fvs.more():
    print(numFrame, numRecognized)
    numFrame += 1
    frame = fvs.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, (CB_HEIGHT,CB_WIDTH), None)
    if ret:
        numRecognized += 1
        objpoints.append(objp)
        corners = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)
        # Draw and display the corners
        cv2.drawChessboardCorners(gray, (CB_HEIGHT,CB_WIDTH), corners, ret)
        cv2.imshow('img', gray)
        key = cv2.waitKey(1000) & 0xFF
        if key == ord('s'):
            filename = 'calibrate_' + str(numScreens) + '.png'
            numScreens += 1
            print("[INFO] writing numScreens to " + filename)
            cv2.imwrite(filename, frame)
print('Closing')
cv2.destroyAllWindows()
