import cv2
import numpy as np
import math
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

# Hard-coded camera index, use 1 if there is an onboard camera.
cap = cv2.VideoCapture(1)

greenLower = (40, 100, 90)
greenUpper = (60, 255, 255)

blueLower = (100, 160, 150)
blueUpper = (115, 255, 255)

orangeLower = (10, 185, 170)
orangeUpper = (25, 255, 255)

def order_points(pts):
	# initialize a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")

	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]

	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]

	# return the ordered coordinates
	return rect

while True:
    ret, frame = cap.read()
    telemetry_computed = True
    orange_center = None
    blue_center = None

    # Find the orange blob.
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, orangeLower, orangeUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if cnts:
        cnts = sorted(cnts, key=cv2.contourArea)
        c = cnts[-1]
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        orange_center = (M["m10"] / M["m00"], M["m01"] / M["m00"])
        cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
        cv2.circle(frame, (int(orange_center[0]), int(orange_center[1])), 5, (0, 0, 255), -1)
    else:
        # Don't report since we don't have the orange blob.
        telemetry_computed = False

    # Find the blue blob.
    if telemetry_computed:
        mask = cv2.inRange(hsv, blueLower, blueUpper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if cnts:
            cnts = sorted(cnts, key=cv2.contourArea)
            c = cnts[-1]
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            blue_center = (M["m10"] / M["m00"], M["m01"] / M["m00"])
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
            cv2.circle(frame, (int(blue_center[0]), int(blue_center[1])), 5, (0, 0, 255), -1)
        else:
            # Don't report since we don't have the blue blob.
            telemetry_computed = False

    # Find the green rectangle.
    if telemetry_computed:
        mask = cv2.inRange(hsv, greenLower, greenUpper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if len(cnts) >= 4:
            cnts = sorted(cnts, key=cv2.contourArea)
            points = []
            for c in cnts[-4:]:
                M = cv2.moments(c)
                center = (M["m10"] / M["m00"], M["m01"] / M["m00"])
                points.append(center)
            rect = order_points(np.array(points))
            cv2.polylines(frame, np.int32([rect]), True, (0,255,255))
            dst = np.array([
                [0, 0],
                [1499, 0],
                [1499, 899],
                [0, 899]], dtype = "float32")
            # Perspective transform transforms our "green rectangle" to the coordinates above, i.e. it assumes our picture contains a perspective on a rectangle define above.
            M = cv2.getPerspectiveTransform(rect, dst)
            # result contains coordinates of orange/blue points in the transformed coordinate system, i.e. inside the wooden arena.
            result = cv2.perspectiveTransform(
                np.array([(orange_center, blue_center)], dtype=np.float32), M)

            orange_x, orange_y = result[0][0][0], result[0][0][1]
            blue_x, blue_y = result[0][1][0], result[0][1][1]
            # orange is back of the car, blue is front of the car
            angle = math.atan2(blue_y - orange_y, blue_x - orange_x)
            print('PUBLISH',
                'telemetry/position',
                ' '.join(map(str, [orange_x, orange_y, angle])))
            publish.single('/'.join(['telemetry', 'position']),
                           payload=' '.join(map(str, [orange_x, orange_y, angle])),
                           hostname='192.168.1.115',
                           port=1883,
                           auth={'username': 'hacklab', 'password': 'balkcah'},
                           protocol=mqtt.MQTTv311)
        else:
            # We need at least 4 points for everything to work.
            telemetry_computed = False

    cv2.imshow('frame', frame)

    pressed = cv2.waitKey(1) & 0xFF
    if pressed == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
