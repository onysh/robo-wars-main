import numpy as np
import argparse
import cv2
import cv2.aruco as aruco
import imutils
from itertools import chain, repeat
 
ap = argparse.ArgumentParser()
ap.add_argument("-vf", type=str, help="Video file to use")
args = vars(ap.parse_args())
 
vs = cv2.VideoCapture(args["vf"])
aruco_dict = aruco.custom_dictionary(6, 3)
#aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)

while True:
    _, frame = vs.read()
    frame = imutils.resize(frame, width=192)

    corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict)
 
    if corners:
        frame = aruco.drawDetectedMarkers(frame, corners, ids)
    else:
        print('problem!')
    cv2.imshow('frame',frame)
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break
 
vs.release()
# When everything done, release the capture
cv2.destroyAllWindows()
