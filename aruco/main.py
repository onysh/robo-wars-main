import numpy as np
import cv2
import cv2.aruco as aruco
from itertools import chain, repeat

NUM_MARKERS = 6
SIDE = 250
aruco_dict = aruco.custom_dictionary(NUM_MARKERS, 3)#aruco.Dictionary_get(aruco.DICT_4X4_50)
print(str(aruco_dict))
# second parameter is id number
# last parameter is total image size
blank = np.ones((SIDE, 10), np.uint8) * 255
img = [aruco.drawMarker(aruco_dict, i, SIDE) for i in range(NUM_MARKERS)]
seq = np.hstack(tuple(chain.from_iterable(zip(repeat(blank), img))))
cv2.imwrite("test_marker.png", seq)

cv2.imshow('frame', seq)
cv2.waitKey(0)
cv2.destroyAllWindows()
