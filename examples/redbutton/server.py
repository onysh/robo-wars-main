#!/usr/bin/env python

import asyncio
import datetime
import random
import websockets
import RPi.GPIO as GPIO

button_state = True

GPIO_PIN = 21

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
   
def get_button_state():
    if (GPIO.input(GPIO_PIN) == 1):
        return False
    else:
        return True

async def time(websocket, path):
    print("Time function start")
    while True:
        state = ''
        if get_button_state() == True:
            state = "Button is pressed"
        else:
            state = "Button is released"
        
        print(state)    
        await websocket.send(state)
        #await asyncio.sleep(0.1)

start_server = websockets.serve(time, '127.0.0.1', 5678)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
