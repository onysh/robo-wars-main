Introduction
============

Backend component. Selects next available request, configures camera and rover, gives control to player. After challenge is completed or time runs out repeats.

config.yaml
===========

mqtt:
  username: ?
  password: ?
  server: ?
  port: ?
mysql:
  host: ?
  user: ?
  password: ?
  db: ?
mosquitto:
  pidfile: /var/run/mosquitto.pid
  passwdfile: /usr/local/etc/mosquitto/pswd.conf
  aclfile: /usr/local/etc/mosquitto/acl.conf
competition:
  startx: 10
  starty: 10
  starta: 0
  slot_duration_min: 5
  auto_skip_after_min: 0.5

Database
========

Uses MySQL database configured in config.yaml Tables are defined in tables.sql and are populated by other components (frontend).

Running
=======

$ python3 backend.py

Workflow
========

1. There is no active run. Raise indication of the need to reset the rover position.
2. Monitor reported rover position. After camera reports rover position in the starting region - remove the indication and proceed.
1. Select the next candidate from queue. If there are none keep checking regularly.
2. Configure broker for the current user: change ACL to camera and rover channels.
3. Switch current run to "in progress" in DB and inform about active player in public meta-channel.
4. Listen to player's commands. If no ACK is detected within timeout - change current run to "done" with corresponding error message.
5. If button is pressed change current run to "done" with corresponding message.
6. If 5 minutes passes change current run to "done" with corresponding message.
7. After the active run is reset through any of the previous scenarios go to start.
