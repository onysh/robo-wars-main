local MQTT = require 'mqtt'

local WIDTH = 50
local HEIGHT = 20

--local MQTT_SERVER = 'm10.cloudmqtt.com'
--local MQTT_PORT = 16860
--local MQTT_LOGIN = 'wahnfosn'
--local MQTT_PASSWORD = '8P7oll9SD0l5'
local MQTT_SERVER = '192.168.1.208'
local MQTT_PORT = 1883
local MQTT_LOGIN = 'merlin'
local MQTT_PASSWORD = 'merlin'
local MQTT_PREFIX = 'redbutton/test/u13'

local WALLS = {
  {-10, -10, 1640, 20},
  {-10, -10, 20, 940},
  {1610, -10, 20, 940},
  {-10, 910, 1640, 20},
  {1080, 200, 20, 720},
}

local KW = 0.5
local KH = 0.5
local OW = 20
local OH = 20

local mqtt_client
local tank = nil

local STOP, FORWARD, BACK, RIGHT, LEFT = 'stop', 'forward', 'back', 'right', 'left'
local command = STOP

function split(str, delim, maxNb)
   -- Eliminate bad cases...
   if string.find(str, delim) == nil then
      return { str }
   end
   if maxNb == nil or maxNb < 1 then
      maxNb = 0    -- No limit
   end
   local result = {}
   local pat = "(.-)" .. delim .. "()"
   local nb = 0
   local lastPos
   for part, pos in string.gfind(str, pat) do
      nb = nb + 1
      result[nb] = part
      lastPos = pos
      if nb == maxNb then
         break
      end
   end
   -- Handle the last field
   if nb ~= maxNb then
      result[nb + 1] = string.sub(str, lastPos)
   end
   return result
end

function callback(topic, payload)
  if topic == MQTT_PREFIX .. "/telemetry/position" then
    local result = split(payload, " ", 4)
    -- result[1] is time
    local x = tonumber(result[2])
    local y = tonumber(result[3])
    local angle = tonumber(result[4] - 90) * math.pi / 180.0
    tank = {x, y, angle}
  end
end

function love.load()
  mqtt_client = MQTT.client.create(MQTT_SERVER, MQTT_PORT, callback)
  mqtt_client:auth(MQTT_LOGIN, MQTT_PASSWORD)
  mqtt_client:connect("Love2D Joystick Client")
  mqtt_client:subscribe({MQTT_PREFIX .. "/telemetry/position"})
end

function love.update(dt)
  local send = nil
  if love.keyboard.isDown('left') then
    if command ~= LEFT then
      send = LEFT
      command = LEFT
    end
  else
    if command == LEFT then
      send = STOP
      command = STOP
    end
  end
  if love.keyboard.isDown('right') then
    if command ~= RIGHT then
      send = RIGHT
      command = RIGHT
    end
  else
    if command == RIGHT then
      send = STOP
      command = STOP
    end
  end
  if love.keyboard.isDown('up') then
    if command ~= FORWARD  then
      send = FORWARD
      command = FORWARD
    end
  else
    if command == FORWARD then
      send = STOP
      command = STOP
    end
  end
  if love.keyboard.isDown('down') then
    if command ~= BACK  then
      send = BACK
      command = BACK
    end
  else
    if command == BACK then
      send = STOP
      command = STOP
    end
  end
  if send and send ~= STOP then
    mqtt_client:publish(MQTT_PREFIX .. "/command", send .. ":300")
    print('sent', send)
  end
  mqtt_client:handler()
end

function love.draw()
  if tank then
    local dx1 = WIDTH * math.cos(tank[3]) / 2
    local dy1 = WIDTH * math.sin(tank[3]) / 2
    local dx2 = -HEIGHT * math.sin(tank[3]) / 2
    local dy2 = HEIGHT * math.cos(tank[3]) / 2
    local x = tank[1]
    local y = tank[2]
    love.graphics.polygon(
        "fill",
        OW + KW*(x + dx1 + dx2), OH + KH*(y + dy1 + dy2),
        OW + KW*(x + dx1 - dx2), OH + KH*(y + dy1 - dy2),
        OW + KW*(x - dx1 - dx2), OH + KH*(y - dy1 - dy2),
        OW + KW*(x - dx1 + dx2), OH + KH*(y - dy1 + dy2))
  end
  for _, wall in ipairs(WALLS) do
    x, y, w, h = unpack(wall)
    x = OW + x * KW
    y = OH + y * KH
    w = w * KW
    h = h * KH
    love.graphics.rectangle("fill", x, y, w, h)
  end
  love.graphics.print("UP = forward", 900, 10)
  love.graphics.print("DOWN = backward", 900, 30)
  love.graphics.print("LEFT = turn left", 900, 50)
  love.graphics.print("RIGHT = turn right", 900, 70)
end
