/**
 * Javascript part of Robowar code
 */

function updateTimerValue(input_data)
{
    var time_value = input_data;
    var seconds = time_value % 60;
    var minutes = Math.floor(time_value / 60);
    var timer_dom = document.getElementById("timer");
    timer_dom.innerHTML = (minutes.toString().padStart(2, "0") + ":"
                           + seconds.toString().padStart(2, "0"));

}
function updateRequestsTable(input_data)
{
    var table_dom = document.getElementById('requests_table');
    var table_data = '<table class="table"><thead><tr><td>Id</td><td>User ID\
    </td><td>status</td><td>requested_time</td><td>Start</td><td>End</td><td>Result</td></tr>\
    </thead><tbody>';
    var current_user = document.getElementById("logged_user").dataset.user;
    console.log(current_user);
    table_object = input_data;

    for (i = 0; i < table_object.length; i++) {
        var id = table_object[i].id;
        var user_id = table_object[i].user_id;
        var status = table_object[i].status;
        var time_requested = table_object[i].requested;
        var start = table_object[i].start;
        var end = table_object[i].end;
        var result = table_object[i].result;


        if (status == "in progress") {
            table_data += '<tr class="blinker"><td>' + id + '</td><td>' +
                user_id + '</td><td>' + status +
                '</td><td>' + time_requested +
                '</td><td>' + start +
                '</td><td>' + end +
                '</td><td>' + result +
                '</td></tr>';
        } else {
            table_data += '<tr><td>' + id + '</td><td>' +
                user_id + '</td><td>' + status +
                '</td><td>' + time_requested +
                '</td><td>' + start +
                '</td><td>' + end +
                '</td><td>' + result +
                '</td></tr>';
        }

    }
    table_data += '</tbody></table>';
    table_dom.innerHTML = table_data;
}

var ws_requests = new WebSocket('ws://' + document.domain + ':' + location.port + '/requests_data');
ws_requests.onmessage = function(event) {
    data = JSON.parse(event.data);

    if (data["type"] == "requests table")
    {
        updateRequestsTable(data["value"]);
    } else if (data["type"] == "set timer")
    {
        updateTimerValue(data["value"]);
    }
};

var ws_users = new WebSocket('ws://' + document.domain + ':' + location.port + '/users_data');
ws_users.onmessage = function(event) {
    var table_dom = document.getElementById('users_table');

    table_object = JSON.parse(event.data);
    console.log(event.data)
    var table_data = '<table class="table"><thead><tr><td>Id</td><td>Name</td><td>Best Result</td></tr></thead><tbody>';
    for (i = 0; i < table_object.length; i++) {
        var id = table_object[i].id;
        var name = table_object[i].username;
        var best_result = table_object[i].best_result;

        table_data += '<tr><td>' + id + '</td><td>' +
                name + '</td><td>' + best_result + '</td></tr>';
    }
    table_data += '</tbody></table>';
    table_dom.innerHTML = table_data;
};

/*
var ws_mqtt = new WebSocket('ws://' + document.domain + ':' + location.port + '/mqtt_data');
ws_mqtt.onmessage = function(event) {
    console.log("Mqtt package received");
    var mqtt_dom = document.getElementsByName('mqttdata')[0];

    mqtt_object = JSON.parse(event.data);
    var mqtt_data = '<ul>';
    for (const [key, value] of Object.entries(mqtt_object)) {
        console.log(key, value);
        mqtt_data += '<li>' + key + ':' + value + '</li>';
    }
    mqtt_data += '</ul>';
    mqtt_dom.innerHTML = mqtt_data;
};
*/

var ws_events = new WebSocket('ws://' + document.domain + ':' + location.port + '/events_channel');
ws_events.onmessage = function(event) {
    console.log("Event received");
    event_object = JSON.parse(event.data);
    console.log(event_object);
    if (event_object['type'] == "set timer")
    {
        console.log("Set timer");
        console.log(event_object["value"]);
    }
};

var button = document.getElementById('request_button');
button.onclick = function() {
    ws_events.send("request time");
};

var button = document.getElementById('skip_button');
button.onclick = function() {
    ws_events.send("skip request");
};