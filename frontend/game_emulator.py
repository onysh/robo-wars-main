#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 14 23:58:55 2018

@author: artsin
"""
import json
from random import randint
import pymysql
import yaml
import os
import datetime
import sys
import time

class DatabaseConnector():
    cfg = None
    def __init__(self, cfg_file): 
        CONFIG_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), cfg_file)

        # Initial setup
        try:
            with open(CONFIG_FILE, 'r') as yamlfile:
                self.cfg = yaml.load(yamlfile)
        except IOError as e:
            print('[PANIC]', 'config loading', str(e))
            sys.exit(1)
        
        self.connection =  pymysql.connect(
                host=self.cfg['mysql']['host'],
                user=self.cfg['mysql']['user'],
                password=self.cfg['mysql']['password'],
                db=self.cfg['mysql']['db'],
                cursorclass=pymysql.cursors.DictCursor,
                autocommit=True)
                   
    def check_user(self, username, password):
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT prefix FROM Users WHERE username = %s", (username))
            value = cursor.fetchone()
            
            print(value)
            if value['prefix'] == password:
                return True
            else:
                return False
    
    def reset_all_data(self):
        with self.connection.cursor() as cursor:
            print("Requests data erased")
            request_time_query_string = ("delete from Requests;")
            cursor.execute(request_time_query_string)
        self.connection.commit()
        
    def start_session(self):
        
        with self.connection.cursor() as cursor:
            request_time_query_string = ("UPDATE Requests SET start=now(), status='in progress' LIMIT 1;")
            cursor.execute(request_time_query_string)
            time.sleep(20)

            request_time_query_string = ("UPDATE Requests SET end=now(), status='done' LIMIT 1;")
            cursor.execute(request_time_query_string)
        self.connection.commit() 
        

        
if __name__ == "__main__":
    print("Test routines")
    
    connector = DatabaseConnector("testconfig_localhost.yml")
    connector.start_session()
