Quickstart
================

1. Setup virtualenv
```
$ python3 -m venv ./env
$ source ./env/bin/activate
```
2. Install deps
```
$ pip3 install -r requirements.txt
```
3. Start flask server
```
$ python3 app.py
```
4. For test streaming run
```
$ ./vlc-strem.sh _name_of_videfile
```

