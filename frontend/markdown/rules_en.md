# Competitor's Guide

[TOC]

## Introduction


### Overview

The goal of the competition is to program an agent that would drive the Rover through the Labyrinth. Rover starts at fixed position and finishes at given position. Labyrinth is fixed, one can find all the information about it in the _Game field_ section.

### Control

The Rover is controlled remotely. All commands are communicated via an MQTT protocol. To be able to control the Rover you should use such MQTT topics:

* *command* topic - the one where challenge agents post their commands.
* *telemetry* topic - the one where challenge infrastructure posts feedback information about the rover.

### Topic names
Topic name consists of:

* prefix - part that identifies whether it is a topic connected to hardware or to an emulator
* suffix - part that identifies topic type

There are two possible prefexes:

* `redbutton` - hardware topic
* `redbutton/test/<userid>` - emulator topic
 * `<userid>` is generated as `u<ID>`, where ID can be found near the user name on the frontend
 * for example u13

There are two possible suffixes:

* `/command` - *command* topic
* `/telemetry/position` - *telemetry* topic

For example:

* commands to hardware rover should be posted in "redbutton/command" topic (without quotes)
* telemetry from simutated rover for user with ID 13 should be read from "redbutton/test/u13/telemetry/position" (without quotes)

### Command topic

Command topic should be used to control the rover. General format for a single command is:

```
<forward|back|left|right>[:<time in ms>]
```

Several commands might be combined into a single command like this:

```
<command>[;<command>]*
```

For example:

```
forward:100
```

```
left:10;forward:100;right:10
```

A sequence of commands (or maybe just one) will be executed on the rover until time for them is elapsed or a new sequence is received.
### Telemetry topic

Telemetry topic gives user feedback information about rover position. Format for a *telemetry* topic message is:
```
<time> <x> <y> <angle>
```

Here:

* `<time>` is a floating point timestamp in seconds, time is local to the camera machine.
* `<x>`, `<y>` are rover coordinates in mm within certain coordinate system
 * coordinates represent center of the marker on top of the rover
 * coordinate system originates at the lower left corner of the field, ask judges for visual cue
* `<angle>` is a non-normalized angle of rotation for the rover in degrees (i.e. we don't give any particular range guarantees)

### Credentials

All MQTT connections within the system are authenticated. One can use following parameters for connection:

* server - 192.168.1.208
* port - 1883
* user - your username
* password - your password

## Game field

### Overview

Green area is the starting point, red area is the position of the Red Button. Numbered points have fixed coordinates associated with them, see _Coordinates_ section.

### Map

<img src="https://i.imgur.com/rDNOoqZ.png" width="75%" />

### Coordinates

Everything is in mm

1. 6, 40
2. 6, 1089
3. 1476, 1089
4. 1476, 40
5. 1476, 356
6. 321, 780
7. 636, 682
8. 636, 504
9. 636, 357
10. 954, 787
11. 1157, 757
12. 1270, 356

## Examples

* [Python](/static/generated/python.html)
