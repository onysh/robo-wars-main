import paho.mqtt.client as mqtt  # needs to be installed
import time
import math

MQTT_PREFIX = 'redbutton/test/u13'
#MQTT_PREFIX = 'redbutton'

def mqttOnConnect(client, userdata, flags, rc):
    client.subscribe(MQTT_PREFIX + '/telemetry/position')

def mqttOnMessage(client, userdata, msg):
    if msg.topic == MQTT_PREFIX + '/telemetry/position':
        print(list(map(float, msg.payload.split())))

client = mqtt.Client()
client.on_connect = mqttOnConnect
client.on_message = mqttOnMessage
client.username_pw_set('merlin', 'password')
client.connect('192.168.1.208', 1883)

client.publish(MQTT_PREFIX + '/command', 'forward:100')
while True:
  client.loop(timeout=1.0)

