# Як це працює?

[TOC]

## Вступ

Мета змагання в тому, щоб запрограмувати агента, який проведе Ровер через Лабіринт. Ровер стартує з визначеної позиції і фінішує на заданій позиції. Лабіринт фіксований, інформація про нього міститься в секції _Поле_

### Контроль

Ровер контролюється віддалено. Усі команди передаються йому через протокол MQTT. Щоб контролювати Ровер, використовуйте такі топіки MQTT:

* *command* topic - в цьому топіку постяться команди для Ровера.
* *telemetry* topic - у цьому топіку поститься інформація про розміщення Ровера.

### Назви топіків
Складові назви топіку:

* префікс - частина, що визначає, чи пов'язаний топік з емулятором чи з реальним залізом
* суфікс - частина, що визначає тип топіку

Є два можливих префікси:

* `redbutton` - залізо
* `redbutton/test/<userid>` - емулятор
 * `<userid>` генерується як `u<ID>`, ID ви можете знайти біля імені користувача на фронтенді 
 * наприклад, u13

Є два можливих суфікси:

* `/command` - команда
* `/telemetry/position` - телеметрія

Наприклад:

* команди до справжнього Ровера мають поститись в топік "redbutton/command" (без лапок)
* телеметрія з симульованого Ровера для користувача з ID 13 читається з топіка "redbutton/test/u13/telemetry/position" (без лапок)

### Командний топік

Командний топік використовується для контролю Ровера. Загальний формат для однієї команди такий:

```
<forward|back|left|right>[:<time in ms>]
```

Кілька команд можна поєднати в одну таким чином:

```
<command>[;<command>]*
```

Наприклад:

```
forward:100
```

```
left:10;forward:100;right:10
```

Команда чи послідовність команд буде виконана на ровері поки не закінчиться виділений на них час або поки не отримана нова послідовність.

### Телеметричний топік

Телеметричний топік дає користувачам фідбек про позицію Ровера. Формат повідомлення:

```
<time> <x> <y> <angle>
```

Пояснення:

* `<time>` час в секундах (число з рухомою комою) 
* `<x>`, `<y>` координати Ровера в міліметрах у межах певної системи координат
 * координати представляють центр мітки вгорі Ровера
 * система координат починається з лівого нижнього кутка поля, судді вам покажуть
* `<angle>` це не-нормалізований кут повороту для Ровера в градусах

### Доступ

Для з'єднання по MQTT використовуйте такі параметри:

* server - 192.168.1.208
* port - 1883
* user - ваш юзернейм
* password - ваш пароль

## Поле

### Огляд

Зелене поле це початок, червона точка - позиція Червоної Кнопки. Номери мають пов'язані координати (див. розділ _Координати_)

### Карта

<img src="https://i.imgur.com/rDNOoqZ.png" width="75%" />

### Координати

Усі координати в міліметрах

1. 6, 40
2. 6, 1089
3. 1476, 1089
4. 1476, 40
5. 1476, 356
6. 321, 780
7. 636, 682
8. 636, 504
9. 636, 357
10. 954, 787
11. 1157, 757
12. 1270, 356

## Приклади

* [Python](/static/generated/python.html)
