#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 14 23:58:55 2018

@author: artsin
"""
import json
from random import randint
import pymysql
import yaml
import os
import datetime
import sys

class DatabaseConnector():
    cfg = None
    def __init__(self, cfg_file):
        CONFIG_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), cfg_file)

        # Initial setup
        try:
            with open(CONFIG_FILE, 'r') as yamlfile:
                self.cfg = yaml.load(yamlfile)
        except IOError as e:
            print('[PANIC]', 'config loading', str(e))
            sys.exit(1)

        self.connection =  pymysql.connect(
                host=self.cfg['mysql']['host'],
                user=self.cfg['mysql']['user'],
                password=self.cfg['mysql']['password'],
                db=self.cfg['mysql']['db'],
                cursorclass=pymysql.cursors.DictCursor,
                autocommit=True)

    def dump_json_users(self):
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT * FROM Users")

            return json.dumps(cursor.fetchall())
    
    def emulator_data(self, username):
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT prefix, password, username FROM Users WHERE username = %s", (username))
            return cursor.fetchone()
        
    def get_json_users_data(self):
        with self.connection.cursor() as cursor:
            cursor.execute("select id, username, best_result from Users left join (select user_id, min(end-start) as best_result from Requests where status = 'success' group by user_id ) as Best on Users.id=Best.user_id order by best_result desc")

            data = cursor.fetchall()
            #print(data)
            #sorted_data = sorted(data, key=lambda k: k['best_result'])
            #try:
            #    sorted_data = sorted(data, key=lambda k: k['best_result'])
            #except:
            #    sorted_data = data

            #print(sorted_data)
            #return json.dumps(data)
            return json.dumps(data)

    def add_user(self, username, password):
        with self.connection.cursor() as cursor:
            sql = "INSERT INTO `Users` (`username`, `password`, `prefix`) VALUES (%s, %s, NULL)"
            cursor.execute(sql, (username, password))

        self.connection.commit()


    def check_user(self, username, password):
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT password FROM Users WHERE username = %s", (username))
            value = cursor.fetchone()
            
            if value is None:
                return False
            
            if value['password'] == password:
                return True
            else:
                return False


    def dump_json_requests(self):
        cursor = self.connection.cursor()

        #rows = cursor.execute("SELECT *, min(end-start) as result FROM Requests")
        rows = cursor.execute("SELECT * FROM Requests ORDER BY requested DESC")

        #return json.dumps(cursor.fetchall())

        rawdata =  cursor.fetchall()

        for index, d in enumerate(rawdata):
            try:
                rawdata[index]['start'] = rawdata[index]['start'].strftime('%m/%d/%Y - %H:%M:%S')
            except AttributeError:
                rawdata[index]['start'] = "-"

            try:
                rawdata[index]['end'] = rawdata[index]['end'].strftime('%m/%d/%Y - %H:%M:%S')
            except AttributeError:
                rawdata[index]['end'] = "-"

            try:
                rawdata[index]['requested'] = rawdata[index]['requested'].strftime('%m/%d/%Y - %H:%M:%S')
            except AttributeError:
                rawdata[index]['requested'] = "-"
        #rawdata["type"] = "requests table"

        #return json.dumps(rawdata)
        return json.dumps({"type": "requests table", "value": rawdata})

    def request_time(self, username):
        # Find user_id
        id_query_string = "SELECT * FROM Users WHERE username = '%s'" % username
        check_query_string = "SELECT user_id FROM Requests WHERE user_id = %s AND (status = 'waiting' OR status = 'in progress')"
        request_time_query_string = "INSERT INTO Requests (user_id, requested, status) VALUES (%s, now(), 'waiting')"
        with self.connection.cursor() as cursor:
            cursor.execute(id_query_string)
            #return json.dumps(cursor.fetchall())
            rawdata = cursor.fetchall()
            user_id = rawdata[0]["id"]
            rows = cursor.execute(check_query_string, (user_id,))
            if rows == 0:
                cursor.execute(request_time_query_string, (user_id,))
        self.connection.commit()
    def skip_request(self, username):
        # Find user_id
        id_query_string = ("SELECT * FROM Users WHERE username = '%s'" % username)
        with self.connection.cursor() as cursor:
            rows = cursor.execute(id_query_string)

            #return json.dumps(cursor.fetchall())

            rawdata =  cursor.fetchall()
            user_id = rawdata[0]["id"]
            request_time_query_string = ("UPDATE Requests "
            "SET status = 'canceled' WHERE user_id = %d AND status = 'in progress'" % user_id)
            cursor.execute(request_time_query_string)
        self.connection.commit()
    def get_runtimer_value(self):
        query_string = ("SELECT * FROM Requests WHERE status = 'in progress'")
        with self.connection.cursor() as cursor:
            cursor.execute(query_string)
            result = cursor.fetchone()
            value = 0
            try:
                delta = datetime.datetime.now() - result["start"]
                value = round(delta.total_seconds())
            except TypeError:
                value = 0
            except:
                print("Unknown exception")
                value = 0
            return_value = json.dumps({"type": "set timer", "value": value})
            return return_value

if __name__ == "__main__":
    print("Test routines")
    connector = DatabaseConnector("testconfig_localhost.yml")
    print(connector.get_json_users_data())
