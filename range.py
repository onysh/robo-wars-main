import cv2
import sys

cap = cv2.VideoCapture(1)

lower = [0, 0, 0]
upper = [255, 255, 255]

if len(sys.argv) > 1:
    a, b, c, d, e, f = map(int, sys.argv[1].split(','))
    lower = [a, b, c]
    upper = [d, e, f]

while(True):
    ret, frame = cap.read()

    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, tuple(lower), tuple(upper))

    cv2.imshow('frame', mask)

    pressed = cv2.waitKey(1) & 0xFF
    if pressed == ord('q'):
        break
    if pressed == ord('w'):
        lower[0] = lower[0] + 1
    if pressed == ord('s'):
        lower[0] = lower[0] - 1
    if pressed == ord('e'):
        lower[1] = lower[1] + 1
    if pressed == ord('d'):
        lower[1] = lower[1] - 1
    if pressed == ord('r'):
        lower[2] = lower[2] + 1
    if pressed == ord('f'):
        lower[2] = lower[2] - 1
    if pressed == ord('y'):
        upper[0] = upper[0] + 1
    if pressed == ord('h'):
        upper[0] = upper[0] - 1
    if pressed == ord('u'):
        upper[1] = upper[1] + 1
    if pressed == ord('j'):
        upper[1] = upper[1] - 1
    if pressed == ord('i'):
        upper[2] = upper[2] + 1
    if pressed == ord('k'):
        upper[2] = upper[2] - 1
    print(lower, upper)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
