import argparse
import csv
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-f", type=str, help="CSV file name.")
ap.add_argument("-hsv", action="store_true", help="Whether to convert to HSV.")
args = vars(ap.parse_args())

data = csv.reader(open(args["f"]))
histogram = []
for i in range(3):
    histogram.append([0] * 256)
for line in data:
    x, y, r = map(int, line[1:])
    r2 = r * r
    filename = line[0]
    frame = cv2.imread(filename)
    height, width = frame.shape[:2]
    if args["hsv"]:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    for row in range(y - r, y + r + 1):
        for col in range(x - r, x + r + 1):
            if (row - y) * (row - y) + (col - x) * (col - x) <= r2:
                if row >= 0  and col >= 0 and row < height and col < width:
                    c1, c2, c3 = frame[row, col]
                    histogram[0][c1] += 1
                    histogram[1][c2] += 1
                    histogram[2][c3] += 1
for i in range(256):
    print(','.join(map(str, [i, histogram[0][i], histogram[1][i], histogram[2][i]])))
