import argparse
import csv
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-histo", type=str, help="CSV histogram.")
ap.add_argument("-f", type=str, help="CSV with input data.")
ap.add_argument("-hsv", action="store_true", help="Whether to convert to HSV.")
ap.add_argument("-onlyranges", action="store_true", help="If set only prints ranges for quantiles.")
ap.add_argument("-iter", type=int, default=3, help="Number of iterations for erode/dilate.")
args = vars(ap.parse_args())

histodata = csv.reader(open(args["histo"]))
histogram = [[], [], []]
count = 0
for line in histodata:
    c1, c2, c3 = map(int, line[1:])
    histogram[0].append(c1)
    histogram[1].append(c2)
    histogram[2].append(c3)
    count += c1

def getRanges(quantile):
    global count, histogram
    tail = count * quantile / 200
    rangeLower = [0, 0, 0]
    rangeUpper = [255, 255, 255]
    for i in range(3):
        lower = 0
        upper = 255
        total = 0
        for j in range(256):
            if total + histogram[i][j] > tail:
                lower = j
                break
            total += histogram[i][j]
        total = 0
        for j in range(255, -1, -1):
            if total + histogram[i][j] > tail:
                upper = j
                break
            total += histogram[i][j]
        rangeLower[i] = lower
        rangeUpper[i] = upper
    return rangeLower, rangeUpper


if args["onlyranges"]:
    for quantile in range(100):
        print(quantile, getRanges(quantile))
    exit(0)

data = csv.reader(open(args["f"]))
for line in data:
    x, y, r = map(int, line[1:])
    r2 = r * r
    filename = line[0]

    frame = cv2.imread(filename)
    height, width = frame.shape[:2]
    if args["hsv"]:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    result = []
    for quantile in range(100):
        rangeLower, rangeUpper = getRanges(quantile)
        mask = cv2.inRange(frame, tuple(rangeLower), tuple(rangeUpper))
        mask = cv2.erode(mask, None, iterations=args["iter"])
        mask = cv2.dilate(mask, None, iterations=args["iter"])
        cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        if cnts:
            cnts = sorted(cnts, key=cv2.contourArea)
            c = cnts[-1]
            M = cv2.moments(c)
            (mx, my) = (M["m10"] / M["m00"], M["m01"] / M["m00"])
            if (mx - x) * (mx - x) + (my - y) * (my - y) <= r2:
                result.append(1)
            else:
                result.append(0)
        else:
            result.append(0)
    print(filename, ',', ','.join(map(str, result)))
