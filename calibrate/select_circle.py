import cv2
import argparse
import glob
import math

ap = argparse.ArgumentParser()
ap.add_argument("files", type=str, help="Pattern of files to read.")
args = vars(ap.parse_args())

orig_frame = None
frame = None
point_down = None
radius = None
last_point_down = None
last_radius = None

def reset_frame():
    global frame, orig_frame, last_point_down, last_radius
    frame = orig_frame.copy()
    if last_point_down:
        cv2.circle(frame, last_point_down, last_radius, (200, 200, 200), 1)

def mouse(event, x, y, flags, param):
        global point_down

        if event == cv2.EVENT_LBUTTONDOWN:
            point_down = (x, y)

        elif event == cv2.EVENT_MOUSEMOVE:
            if point_down is not None:
                reset_frame()
                global frame, radius
                radius = math.sqrt(
                        (point_down[0] - x) * (point_down[0] - x) +
                        (point_down[1] - y) * (point_down[1] - y))
                cv2.circle(frame, point_down, int(radius), (0, 255, 0), 1)
                cv2.imshow('image', frame)
            else:
                reset_frame()
                cv2.circle(frame, (x, y), 1, (0, 0, 255), -1)
                cv2.imshow('image', frame)

        elif event == cv2.EVENT_LBUTTONUP:
            global raidus, last_point_down, last_radius
            last_point_down = point_down
            last_radius = int(radius)
            point_down = None

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.setMouseCallback('image', mouse)
files = glob.glob(args["files"])
for filename in files:
    orig_frame = cv2.imread(filename)
    reset_frame()
    cv2.imshow('image', frame)
    while True:
        key = cv2.waitKey(5) & 0xFF
        if key == ord("s"):
            print(','.join(map(str, [
                filename,
                last_point_down[0],
                last_point_down[1],
                last_radius])))
            break
        if key == ord("d"):
            break
cv2.destroyAllWindows()
