import cv2
import argparse
import numpy as np

ap = argparse.ArgumentParser()
ap.add_argument('-image', type=str, help='File to analyze')
args = vars(ap.parse_args())

image = cv2.imread(args['image'])
height, width = image.shape[:2]
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# This is for finding suitable lower threshold: I picked 135
#for lower in range(125, 150):
#    mask = cv2.inRange(image, lower, 255)
#    cv2.imshow('image', mask)
#    print(lower)
#    cv2.waitKey(0)

mask = cv2.inRange(gray, 135, 255)
mask = cv2.erode(mask, None, iterations=2)
mask = cv2.dilate(mask, None, iterations=2)
cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
cnt_number = 0
to_show = []
for cnt in cnts:
    if cv2.contourArea(cnt) > 10000:
        approx = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)
        if len(approx) == 4:
            cnt_number += 1
            to_show.append((approx[0][0][0], approx[0][0][1], cnt_number))
            mask = np.zeros((height, width), np.uint8)
            cv2.drawContours(mask, [approx], 0, 255, -1)
            for i in range(3):
                hist_mask = cv2.calcHist([image], [i], mask, [256], [0, 256])
                print(','.join(map(str, [cnt_number, i] + list(map(int, (h[0] for h in hist_mask))))))

image = cv2.resize(image, (width//8, height//8), interpolation = cv2.INTER_CUBIC)
for t in to_show:
    x = t[0] // 8
    y = t[1] // 8
    cv2.putText(image, str(t[2]), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
cv2.imshow('image', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
