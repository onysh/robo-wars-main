import time
import yaml
import math

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import cv2
import cv2.aruco as aruco
import numpy as np
from imutils.video import WebcamVideoStream

from flask import Flask, render_template, Response
from werkzeug.serving import run_simple

CONFIG_FILE = 'config.yaml'

# Initial setup
try:
    with open(CONFIG_FILE, 'r') as yamlfile:
        cfg = yaml.load(yamlfile)
except IOError as e:
    print("Exception", str(e))
    sys.exit(1)

#app = Flask(__name__)
#result = None
#
#@app.route('/')
#def index():
#    return render_template('index.html')
#
#def gen():
#    global vs, aruco_dict
#    print('HOW MUCH')
#    while True:
#        result = vs.read()
#        corners, ids, rejectedImgPoints = aruco.detectMarkers(result, aruco_dict)
#        if len(corners) == 1:
#            result = aruco.drawDetectedMarkers(result, corners, ids)
#            publish.single('/'.join(['redbutton', 'telemetry', 'position']),
#                    payload=' '.join(map(str, [time.time(), result[0][0][0], result[0][0][1], 0.0])),
#                    hostname=cfg['mqtt']['server'],
#                    port=cfg['mqtt']['port'],
#                    auth={'username': cfg['mqtt']['username'], 'password': cfg['mqtt']['password']},
#                    protocol=mqtt.MQTTv311)
#        else:
#            #result = aruco.drawDetectedMarkers(result, rejectedImgPoints)
#            ret, jpeg = cv2.imencode('.jpg', result)
#            yield (b'--frame\r\n'
#                   b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')
#
#@app.route('/video_feed')
#def video_feed():
#    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')
#
#run_simple('0.0.0.0', 5000, app, use_reloader=False)
#print('Server started')

def center(marker):
    summ = marker[0]
    for i in range(1, 4):
        summ += marker[i]
    return summ / 4.0

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

aruco_dict = aruco.custom_dictionary(6, 3)

cam = WebcamVideoStream(src=int(cfg['camera']['src']))
cam.stream.set(cv2.CAP_PROP_FPS, cfg['camera']['fps'])
cam.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
vs = cam.start()

topleft = [0.0, 0.0]
botleft = [0.0, 0.0]
topright = [0.0, 0.0]
botright = [0.0, 0.0]
rover = [0.0, 0.0]
reported = 0


# My kitchen
points = [
        np.array([(0,0,0), (35,0,0), (35,-35,0), (0,-35,0)], ('float32')),
        np.array([(212,0,0), (247,0,0), (247,-35,0), (212,-35,0)], ('float32')),
        np.array([(212,-312,0), (247,-312,0), (247,-347,0), (212,-347,0)], ('float32')),
        np.array([(0,-312,0), (35,-312,0), (35,-347,0), (0,-347,0)], ('float32'))
        ]

## Production
#points = [
#        np.array([(0,571,0), (40,571,0), (40,531,0), (0,531,0)], ('float32')),
#        np.array([(905,1142,0), (945,1142,0), (945,1102,0), (905,1102,0)], ('float32')),
#        np.array([(1810,571,0), (1850,571,0), (1850,531,0), (1810,531,0)], ('float32')),
#        np.array([(905,40,0), (945,40,0), (945,0,0), (905,0,0)], ('float32'))
#        ]

# My kitchen
ids = np.array([[1], [2], [3], [5]])
# rover = 0, debug = 4

## Production
#ids = np.array([[1], [0], [3], [2]])
## rover = 5, debug = 4

# My kitchen
markerLength = 35
roverLength = 88

board = aruco.Board_create(points, aruco_dict, ids)
print(aruco_dict.bytesList)
print(aruco_dict.markerSize)
print(aruco_dict.maxCorrectionBits)

# Wide camera from charuco
camera_matrix = np.array([[629.91047542, 0., 360.00000001], [0., 578.24292038, 640.00000001], [0., 0., 1.]], dtype='float32')
dist_coeffs = np.array([[1.41791875e-10, 1.11055690e-09, 1.47704626e-10, -2.20223719e-11, 1.01112518e-08]], dtype='float32')

inited = False
while True:
    result = vs.read()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(result, aruco_dict)
    if ids is not None and len(ids) > 0:
        retval, rvec, tvec = aruco.estimatePoseBoard(corners, ids, board, camera_matrix, dist_coeffs)
        if retval != 0:
            inited = True
            rmat, _ = cv2.Rodrigues(rvec)
            homogen = np.vstack((np.hstack((rmat, tvec)),
                                 np.array([0., 0., 0., 1.])))
            camera_to_board = np.linalg.inv(homogen)
            board_rvec = rvec
            board_tvec = tvec

        # Debug block
        reported += 1
        if inited and reported % 100 == 20:
            im_with_aruco_board = aruco.drawDetectedMarkers(result, corners, ids, (0,255,0))
            im_with_aruco_board = aruco.drawAxis(im_with_aruco_board, camera_matrix, dist_coeffs, board_rvec, board_tvec, 60)
            cv2.imwrite("undetected.png", im_with_aruco_board)
            print("Reported", retval)

        for index, marker in enumerate(ids):
            if inited and marker == 0:
                # Rover
                rvec, tvec, _ = aruco.estimatePoseSingleMarkers(
                        corners[index], roverLength, camera_matrix, dist_coeffs)
                rmat, _ = cv2.Rodrigues(rvec)
                rover_to_camera = np.vstack((np.hstack((rmat,
                                                        tvec[0].transpose())),
                                             np.array([0., 0., 0., 1.])))
                position = np.array([[0., 0., 0., 1.],
                                   [roverLength / 2., 0., 0., 1.]]).transpose()
                res = np.matmul(camera_to_board, np.matmul(rover_to_camera, position))
                print('points', position)
                print('result', res)
                if inited and reported % 100 == 20:
                    pt, _ = cv2.projectPoints(
                            np.float32([
                                [res[0][0], res[1][0], res[2][0]],
                                [res[0][1], res[1][1], res[2][1]],
                                [0, 0, 0],
                                [35, 0, 0],
                                [35, -35, 0],
                                [0, -35, 0],
                                [212, -312, 0],
                                [247, -312, 0],
                                [247, -347, 0],
                                [212, -347, 0],
                                [212, 0, 0],
                                [247, 0, 0],
                                [247, -35, 0],
                                [212, -35, 0],
                                [0, -312, 0],
                                [35, -312, 0],
                                [35, -347, 0],
                                [0, -347, 0]
                            ]), board_rvec, board_tvec, camera_matrix, dist_coeffs)
                    print('display', pt)
                    cv2.circle(im_with_aruco_board, (int(pt[0][0][0]), int(pt[0][0][1])), int(3), (0, 255, 0), -1)
                    cv2.circle(im_with_aruco_board, (int(pt[1][0][0]), int(pt[1][0][1])), int(3), (0, 255, 255), -1)
                    for i in range(2, 18):
                        cv2.circle(im_with_aruco_board, (int(pt[i][0][0]), int(pt[i][0][1])), int(3), (255, 0, 0), -1)
                    cv2.imwrite("undetected.png", im_with_aruco_board)
                    print('>'*50)
                break


        #needed = 0
        #rover = False
        #for index, marker in enumerate(ids):
        #    if marker == 1:
        #        A = center(corners[index][0])
        #        needed += 1
        #    elif marker == 0:
        #        D = center(corners[index][0])
        #        needed += 1
        #    elif marker == 2:
        #        B = center(corners[index][0])
        #        needed += 1
        #    elif marker == 3:
        #        C = center(corners[index][0])
        #        needed += 1
        #    elif marker == 5:
        #        rover = True
        #        F = (corners[index][0][0] + corners[index][0][1]) / 2.
        #        R = center(corners[index][0])
        #if needed == 4:
        #    zero = A - (C - A) * 20. / 1810. - (D - B) * 551. / 1102.
        #    y = A - (C - A) * 20. / 1810.
        #    inited = True
        #if inited and rover:
        #    direction = angle_between(R - zero, y - zero)
        #    magnitude = np.linalg.norm(R - zero)
        #    offset = (C-A) * 73. / 1810. + (D-B) * 76. / 1102.
        #    scalex = 1810. / np.linalg.norm(C - A)
        #    scaley = 1102. / np.linalg.norm(D - B)
        #    roverx = (magnitude * math.sin(direction) - offset[0]) * scalex
        #    rovery = (magnitude * math.cos(direction) + offset[1]) * scaley
        #    direction = angle_between(F - zero, y - zero)
        #    magnitude = np.linalg.norm(F - zero)
        #    forwardx = (magnitude * math.sin(direction) - offset[0]) * scalex
        #    forwardy = (magnitude * math.cos(direction) + offset[1]) * scaley
        #    angle = math.atan2(forwardy - rovery, forwardx - roverx) * 180. / math.pi
        #    print(R, F, roverx, rovery, forwardx, forwardy, angle)
        #    if reported % 100 == 20:
        #        cv2.circle(im_with_aruco_board, (int(zero[0]), int(zero[1])), int(3), (0, 0, 255), -1)
        #        cv2.circle(im_with_aruco_board, (int(zero[0] + offset[0] + roverx/scalex), int(zero[1] - offset[1] - rovery/scaley)), int(3), (0, 255, 0), -1)
        #        cv2.imwrite("undetected.png", im_with_aruco_board)
        #        print('>'*50)

        #publish.single('/'.join(['redbutton', 'telemetry', 'position']),
        #        payload=' '.join(map(str, [time.time(), roverx, rovery, angle])),
        #        hostname=cfg['mqtt']['server'],
        #        port=cfg['mqtt']['port'],
        #        auth={'username': cfg['mqtt']['username'], 'password': cfg['mqtt']['password']},
        #        protocol=mqtt.MQTTv311)
