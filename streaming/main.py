#!/usr/bin/env python
#
# Project: Video Streaming with Flask
# Author: Log0 <im [dot] ckieric [at] gmail [dot] com>
# Date: 2014/12/21
# Website: http://www.chioka.in/
# Description:
# Modified to support streaming out with webcams, and not just raw JPEGs.
# Most of the code credits to Miguel Grinberg, except that I made a small tweak. Thanks!
# Credits: http://blog.miguelgrinberg.com/post/video-streaming-with-flask
#
# Usage:
# 1. Install Python dependencies: cv2, flask. (wish that pip install works like a charm)
# 2. Run "python main.py".
# 3. Navigate the browser to the local webpage.
from flask import Flask, render_template, Response
from camera import VideoCamera
import cv2
import cv2.aruco as aruco
import numpy as np

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

def gen(camera):
    camera_matrix = np.array([[629.91047542, 0., 360.00000001], [0., 578.24292038, 640.00000001], [0., 0., 1.]], dtype='float32')
    dist_coeffs = np.array([[1.41791875e-10, 1.11055690e-09, 1.47704626e-10, -2.20223719e-11, 1.01112518e-08]], dtype='float32')

    ## My kitchen
    #points = [
    #        np.array([(0,0,0), (35,0,0), (35,-35,0), (0,-35,0)], ('float32')),
    #        np.array([(212,0,0), (247,0,0), (247,-35,0), (212,-35,0)], ('float32')),
    #        np.array([(212,-312,0), (247,-312,0), (247,-347,0), (212,-347,0)], ('float32')),
    #        np.array([(0,-312,0), (35,-312,0), (35,-347,0), (0,-347,0)], ('float32'))
    #        ]
    #ids = np.array([[1], [2], [3], [5]])

    # Production
    aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
    points = [
        np.array([(-93,495,0), (-53,495,0), (-53,455,0), (-93,455,0)], ('float32')),
        np.array([(832,1066,0), (872,1066,0), (872,1046,0), (832,1046,0)], ('float32')),
        np.array([(1737,495,0), (1777,495,0), (1777,455,0), (1737,455,0)], ('float32')),
        np.array([(832,-36,0), (872,-36,0), (872,-76,0), (832,-76,0)], ('float32'))
        ]
    ids = np.array([[0], [1], [2], [3]])

    board = aruco.Board_create(points, aruco_dict, ids)

    # 18mm -> 36mm
    loaded = False
    while True:
        orig = camera.get_frame()
        frame = orig.copy()
        corners, mids, rejectedImgPoints = aruco.detectMarkers(orig, aruco_dict)
        #retval, rvec, tvec = aruco.estimatePoseBoard(corners, mids, board, camera_matrix, dist_coeffs)
        frame = aruco.drawDetectedMarkers(frame, corners, mids, (0,255,0))
        for index, marker in enumerate(mids):
            if marker == 3:
                loaded = True
                board_rvec, board_tvec, _ = aruco.estimatePoseSingleMarkers(
                        corners[index], 40, camera_matrix, dist_coeffs)
                break
        if loaded:
            frame = aruco.drawAxis(frame, camera_matrix, dist_coeffs, board_rvec, board_tvec, 50)
            pt, _ = cv2.projectPoints(
                    np.float32([
                        [-93 -852, 495 +56, 0],
                        [-53 -852, 495 +56, 0],
                        [-53 -852, 455 +56, 0],
                        [-93 -852, 455 +56, 0],
                        [832 -852, 1066+56, 0],
                        [872 -852, 1066+56, 0],
                        [872 -852, 1046+56, 0],
                        [832 -852, 1046+56, 0],
                        [1737-852, 495 +56, 0],
                        [1777-852, 495 +56, 0],
                        [1777-852, 455 +56, 0],
                        [1737-852, 455 +56, 0],
                        [832 -852, -36 +56, 0],
                        [872 -852, -36 +56, 0],
                        [872 -852, -76 +56, 0],
                        [832 -852, -76 +56, 0]
                    ]), board_rvec, board_tvec, camera_matrix, dist_coeffs)
            for i in range(16):
                cv2.circle(frame, (int(pt[i][0][0]), int(pt[i][0][1])), int(3), (255, 0, 0), -1)

        #found = False
        #if ids is not None and len(ids) > 0:
        #    for i, m in enumerate(ids):
        #        if m == 14:
        #            rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners[i], 18, camera_matrix, dist_coeffs)
        #            found = True
        #            frame = aruco.drawAxis(frame, camera_matrix, dist_coeffs, rvec, tvec, 20) 
        #            pt, _ = cv2.projectPoints(np.float32([[18., 18., 0.]]), rvec, tvec, camera_matrix, dist_coeffs)
        #            cv2.circle(frame, (int(pt[0][0][0]), int(pt[0][0][1])), 3, (0, 0, 255), -1)
        #            rmat, _ = cv2.Rodrigues(rvec)
        #            homogen = np.vstack((np.hstack((rmat,
        #                                            tvec[0].transpose())),
        #                                 np.array([0., 0., 0., 1.])))
        #            #pt = np.matmul(homogen, np.array([[18., 18., 0., 1.]]).transpose()).transpose()
        #            #cv2.circle(frame, (int(pt[0][0]), int(pt[0][1])), 3, (0, 0, 255), -1)
        #if found:
        #        for i, m in enumerate(ids):
        #            if m == 16:
        #                rvec2, tvec2, _ = aruco.estimatePoseSingleMarkers(
        #                        corners[i], 18, camera_matrix, dist_coeffs)
        #                frame = aruco.drawAxis(frame, camera_matrix, dist_coeffs, rvec2, tvec2, 10) 
        #                rmat2, _ = cv2.Rodrigues(rvec2)
        #                homogen2 = np.vstack((np.hstack((rmat2,
        #                                                 tvec2[0].transpose())),
        #                                      np.array([0., 0., 0., 1.])))
        #                # Column vector
        #                point = np.array([[-18., 0., 0., 1.]]).transpose()
        #                inv = np.linalg.inv(homogen)
        #                res = np.matmul(inv, np.matmul(homogen2, point))
        #                print('result', res)
        #                pt, _ = cv2.projectPoints(np.float32([[res[0][0], res[1][0], res[2][0]]]), rvec, tvec, camera_matrix, dist_coeffs)
        #                print('pt', pt)
        #                cv2.circle(frame, (int(pt[0][0][0]), int(pt[0][0][1])), 3, (0, 255, 255), -1)
        #                ## Get point in camera coords
        #                #pt, _ = cv2.projectPoints(np.float32([[0., 0., 0.]]), rvec2, tvec2, camera_matrix, dist_coeffs)
        #                ## Inverse the first transform
        #                #rmat3, _ = cv2.Rodrigues(rvec)
        #                #rvec3, _ = cv2.Rodrigues(rmat3.transpose())
        #                #tvec3 = -np.matmul(rmat3.transpose(), tvec[0].transpose())
        #                #print('Input', rmat3.transpose(), tvec[0].transpose())
        #                #print('First', rvec, tvec)
        #                #print('Second', rvec2, tvec2)
        #                #rvec3 = [rvec3.transpose()]
        #                #tvec3 = [tvec3.transpose()]
        #                #print('Third', rvec3, tvec3)
        #                #print('Points', pt[0])
        #                #pt3, _ = cv2.projectPoints(pt[0], np.array(rvec3), np.array(tvec3), camera_matrix, dist_coeffs)
        #                #print(pt3)

        ret, jpeg = cv2.imencode('.jpg', frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
