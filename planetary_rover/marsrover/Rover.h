/*
 * Rover.h
 *
 *  Created on: Feb 20, 2018
 *      Author: artsin
 */

#ifndef ROVER_H_
#define ROVER_H_

#include "Arduino.h"

class Rover
{
public:
	Rover(uint8_t pinLeftOne, uint8_t pinLeftTwo, uint8_t pinRightOne,
			uint8_t pinRightTwo);
	~Rover();

	void moveForward();
	void moveBack();
	void moveRight();
	void moveLeft();
	void stop();

protected:
	uint8_t pinLeftOne;
	uint8_t pinLeftTwo;
	uint8_t pinRightOne;
	uint8_t pinRightTwo;
};

#endif /* ROVER_H_ */
