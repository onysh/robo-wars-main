/*
 * Rover.cpp
 *
 *  Created on: Feb 20, 2018
 *      Author: artsin
 */

#include "Rover.h"

Rover::Rover(uint8_t pinLeftOne, uint8_t pinLeftTwo, uint8_t pinRightOne,
		uint8_t pinRightTwo) :
		pinLeftOne(pinLeftOne), pinLeftTwo(pinLeftTwo), pinRightOne(
				pinRightOne), pinRightTwo(pinRightTwo)
{

	pinMode(pinLeftOne, OUTPUT);
	pinMode(pinLeftTwo, OUTPUT);
	pinMode(pinRightOne, OUTPUT);
	pinMode(pinRightTwo, OUTPUT);

}

Rover::~Rover()
{

}

void Rover::moveForward()
{
	digitalWrite(pinLeftOne, 0);
	digitalWrite(pinLeftTwo, 1);
	digitalWrite(pinRightOne, 1);
	digitalWrite(pinRightTwo, 0);
}
void Rover::moveBack()
{
	digitalWrite(pinLeftOne, 1);
	digitalWrite(pinLeftTwo, 0);
	digitalWrite(pinRightOne, 0);
	digitalWrite(pinRightTwo, 1);
}
void Rover::moveRight()
{
	digitalWrite(pinLeftOne, 1);
	digitalWrite(pinLeftTwo, 0);
	digitalWrite(pinRightOne, 1);
	digitalWrite(pinRightTwo, 0);
}
void Rover::moveLeft()
{
	digitalWrite(pinLeftOne, 0);
	digitalWrite(pinLeftTwo, 1);
	digitalWrite(pinRightOne, 0);
	digitalWrite(pinRightTwo, 1);
}

void Rover::stop()
{
	digitalWrite(pinLeftOne, 0);
	digitalWrite(pinLeftTwo, 0);
	digitalWrite(pinRightOne, 0);
	digitalWrite(pinRightTwo, 0);
}
