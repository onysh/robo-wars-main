#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "Rover.h"
#include "Settings.h"
#include <queue>

enum states
{
	STOP, BACK, FORWARD, LEFT, RIGHT, RUNNING, QUEUE_PROCESSING, WAIT
};

enum commands
{
	COMMAND_STOP, COMMAND_BACK, COMMAND_FORWARD, COMMAND_LEFT, COMMAND_RIGHT
};

struct command_t
{
	unsigned int command;
	unsigned int duration;
	unsigned int id;
};

WiFiClient espClient;
PubSubClient client(espClient);
Rover rover(MOTOR_ONE_FORWARD, MOTOR_ONE_REVERSE, MOTOR_TWO_FORWARD,
		MOTOR_TWO_REVERSE);

std::queue<command_t> commandQueue;
/* State machine values */
unsigned int current_state = WAIT;

void setup()
{
	pinMode(BUILTIN_LED, OUTPUT);
	Serial.begin(115200);
	setupWiFi();
	client.setServer(mqtt_server, mqtt_port);
	client.setCallback(onMsgReceived);
}

void setupWiFi()
{
	delay(10);
	// We start by connecting to a WiFi network
	Serial.printf("Connecting to %s\n", ssid);

	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}

void onMsgReceived(char* topic, byte* payload, unsigned int length)
{
	String payloadStr = String((char*) payload);
	payloadStr.remove(length);

	Serial.printf("Message arrived [%s]:", topic);
	Serial.println(payloadStr);

	/* Parsing input data on substrings */

	unsigned int ind_start = 0;
	unsigned int ind_end = 0;
	unsigned int command_number = 0;
	unsigned int split_index;
	const unsigned int max_commands = 10;
	int value = 0;
	while (ind_end <= payloadStr.length())
	{
		command_t commandQueueEntry;
		Serial.println(ind_end);
		ind_end = payloadStr.indexOf(';', ind_start + 1);
		String substring = payloadStr.substring(ind_start, ind_end);
		ind_start = ind_end + 1;


		split_index = substring.indexOf(':');
		String command = substring.substring(0, split_index);
		String valueStr = substring.substring(split_index + 1,
				substring.length());
		value = valueStr.toInt();

		if (command == "left")
		{
			Serial.println("Parsed left");
			commandQueueEntry.command = COMMAND_LEFT;
			commandQueueEntry.duration = value;
		}
		else if (command == "right")
		{
			Serial.println("Parsed right");
			commandQueueEntry.command = COMMAND_RIGHT;
			commandQueueEntry.duration = value;
		}
		else if (command == "back")
		{
			Serial.println("Parsed back");
			commandQueueEntry.command = COMMAND_BACK;
			commandQueueEntry.duration = value;
		}
		else if (command == "forward")
		{
			Serial.println("Parsed forward");
			commandQueueEntry.command = COMMAND_FORWARD;
			commandQueueEntry.duration = value;
		}
		else if (command == "stop")
		{
			Serial.println("Parsed stop");
			commandQueueEntry.command = COMMAND_STOP;
			commandQueueEntry.duration = value;
		}

		commandQueue.push(commandQueueEntry);
		if ((command_number++) > max_commands)
			break;
	}
	current_state = QUEUE_PROCESSING;
	Serial.println("===== QUEUE INFO =======");
	Serial.printf("Length: %d\n", commandQueue.size());
	Serial.println("========================");
}

void reconnect()
{
	// Loop until we're reconnected
	while (!client.connected())
	{
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (client.connect("MarsRover", mqtt_username, mqtt_pass))
		{
			Serial.println("connected");
			// Subscribe topics
			client.subscribe("redbutton/command");
		}
		else
		{
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

unsigned int commandToState(unsigned int inputCommand)
{
	unsigned int state;
	Serial.printf("Input command:%d\n", inputCommand);
	switch (inputCommand) {
	case COMMAND_RIGHT:
		state = RIGHT;
		break;
	case COMMAND_LEFT:
		state = LEFT;
		break;
	case COMMAND_BACK:
		state = BACK;
		break;
	case COMMAND_FORWARD:
		state = FORWARD;
		break;
	case COMMAND_STOP:
		state = STOP;
		break;
	default:
		state = WAIT;
		break;
	}
	return state;
}

void loop()
{
	static int counter = 0;
	static unsigned long command_endtime = 0;
	/* MQTT stuff */
	if (!client.connected())
	{
		reconnect();
	}

	client.loop();
	/* State machine stuff */
	switch (current_state)
	{
	case LEFT:
		Serial.println("Move left state");
		current_state = RUNNING;
		rover.moveLeft();
		break;
	case RIGHT:
		Serial.println("Move right state");
		rover.moveRight();
		current_state = RUNNING;
		break;
	case BACK:
		Serial.println("Move back state");
		rover.moveBack();
		current_state = RUNNING;
		break;
	case FORWARD:
		Serial.println("Move forward state");
		rover.moveForward();
		current_state = RUNNING;
		break;
	case STOP:
		Serial.println("Stop state");
		rover.stop();
		current_state = RUNNING;
		break;
	case RUNNING:
		//counter--;
		//counter = counter - (millis() - last_time);
		//if (counter <= 0) {
		if (millis() > command_endtime) {
			Serial.printf("Go back to queue processing, counter = %d\n", counter);
			current_state = QUEUE_PROCESSING;
		}
		break;
	case QUEUE_PROCESSING:
		Serial.println(">>>>Queue processing state");
		Serial.printf("Commands in queue: %d\n", commandQueue.size());
		command_t currentCommand;
		if (commandQueue.size() > 0) {
			currentCommand = commandQueue.front();
			commandQueue.pop();
			current_state = commandToState(currentCommand.command);
			//counter = currentCommand.duration;
			command_endtime = millis() + currentCommand.duration;
			Serial.printf("command: %d, counter: %d\n", currentCommand.command, counter);
		} else {
			rover.stop();
			current_state = WAIT;
			Serial.println("Go to WAIT");
		}

		break;
	case WAIT:
		//Serial.println("Wait");
		break;
	}
	//last_time = millis();
	//delay(1);
}
